
***Main repo moved to:*** https://dev.sect0uch.world/SecT0uch/HomeLab

# Homelab

## Installation

### Initial setup

#### First Steps

- Install Centos
- Setup passwords
- Edit .ssh/authorized_keys
- Change SSH port
- Change firewalld rule:
  - `sudo cp /usr/lib/firewalld/services/ssh.xml /etc/firewalld/services/`
  - Edit `/etc/firewalld/services/ssh.xml` to change port
  - Close dhcpv6-client port : `sudo firewall-cmd --remove-service=dhcpv6-client  --permanent`
  - `sudo firewall-cmd --reload`
- Restart SSHD service : `sudo systemctl restart sshd`
- Update

#### Fail2Ban

- Install fail2ban : `sudo yum install fail2ban fail2ban-systemd`
- Create config (ex : sshd.local)

#### Docker

- Add docker repo and install it : https://docs.docker.com/install/linux/docker-ce/centos/
- Install docker-compose : https://docs.docker.com/compose/install/

#### Nginx

- Add Nginx repo and install it : http://nginx.org/en/linux_packages.html#RHEL-CentOS
- Open ports 80 and 443 :
  - `firewall-cmd --add-service=http --permanent`
  - `firewall-cmd --add-service=https --permanent`
  - `sudo firewall-cmd --reload`

#### SSL certificate

**As root, not sudo :**
- Install acme.sh : https://github.com/Neilpang/acme.sh
- Issue a new certificate : 
  ```sh
  export Namesilo_Key="xxxxxxxxxxxxxxxxxxxxxxxx"`
  acme.sh --issue -d sect0uch.world -d '*.sect0uch.world' --keylength ec-384 --dns dns_namesilo --dnssleep 900
  ```
> NB : Do not use `--nginx` argument, it implies HTTP challenge and we want a DNS challenge to get a wildcard.
  
- Create required directories : `mkdir /etc/nginx/certs && mkdir /etc/nginx/certs/sect0uch.world`
- Generate dhparam.pem file with`openssl dhparam -out /etc/nginx/certs/sect0uch.world/dhparam.pem 4096`
- Edit `/etc/nginx/conf.d/default.conf` [Check file](nginx/default.conf)
- Install the certificates `acme.sh --install-cert -d sect0uch.world --ecc --cert-file /etc/nginx/certs/sect0uch.world/cert.pem --key-file /etc/nginx/certs/sect0uch.world/key.pem --ca-file /etc/nginx/certs/sect0uch.world/ca.pem --fullchain-file /etc/nginx/certs/sect0uch.world/fullchain.pem --reloadcmd "systemctl restart nginx.service"`
- To activate email notifications : 
  ```sh
  export  MAILGUN_API_KEY="xxxxxxxx"
  export  MAILGUN_API_DOMAIN="sect0uch.world"
  export  MAILGUN_FROM="acme@sect0uch.world"
  export  MAILGUN_TO="pro.ernst@gmail.com"
  export  MAILGUN_REGION="eu"
  acme.sh --set-notify  --notify-level 2 --notify-hook  mailgun
  ```

### Mailcow

#### Pre-requisites

Open the required ports with :
```sh
firewall-cmd --add-service=smtp --permanent
firewall-cmd --add-service=smtp-submission --permanent
firewall-cmd --add-service=imap --permanent
firewall-cmd --add-service=managesieve --permanent
sudo firewall-cmd --reload
```

#### Installation

Follow the installation described [here](https://mailcow.github.io/mailcow-dockerized-docs/i_u_m_install/).

#### Configuration
[Disable IPv6](https://mailcow.github.io/mailcow-dockerized-docs/firststeps-disable_ipv6/).

[Use our own certificate](https://github.com/Neilpang/acme.sh/wiki/deployhooks#15-deploy-your-cert-to-local-mailcow-server)
Do not forget to add the following line in `/root/.bashrc` : `export DEPLOY_MAILCOW_PATH="/opt/mailcow-dockerized"`

[Use mailgun as a relay](https://mailcow.github.io/mailcow-dockerized-docs/firststeps-relayhost/).

### FreshRSS

As root :
```sh
mkdir /opt/FreshRSS && cd /opt/FreshRSS
mkdir data
wget https://framagit.org/SecT0uch/homelab/raw/master/FreshRSS/docker-compose.yml
docker-compose run -d
cd /etc/nginx/conf.d
wget https://framagit.org/SecT0uch/homelab/raw/master/nginx/freshrss.conf
systemctl reload nginx
```

### Shell In A Box

To get a shell when you can't SSH out of a LAN.

Install `shellinabox` package.

Edit `/etc/sysconfig/shellinaboxd` :
```sh
# Shell in a box daemon configuration
# For details see shellinaboxd man page

# Basic options
USER=shellinabox
GROUP=shellinabox
CERTDIR=/var/lib/shellinabox
PORT=4222
OPTS="--localhost-only --disable-ssl --disable-ssl-menu --css=/usr/share/shellinabox/white-on-black.css -s /:LOGIN"


# Additional examples with custom options:

# Fancy configuration with right-click menu choice for black-on-white:
# OPTS="--user-css Normal:+black-on-white.css,Reverse:-white-on-black.css --disable-ssl-menu -s /:LOGIN"

# Simple configuration for running it as an SSH console with SSL disabled:
# OPTS="-t -s /:SSH:host.example.com"
```

Add the [corresponding nginx configuartion file](nginx/shellinabox.conf), edit the "allow" line, to add the addresses you want to whitelist.

Enable and start the service, and reload nginx : `systemctl start shellinboxd && systemctl enable shellinaboxd && systemctl reload nginx`

## Update

### Mailcow

https://mailcow.github.io/mailcow-dockerized-docs/i_u_m_update/

### FreshRSS

```sh
docker-compose pull
docker-compose stop
docker-compose up -d
```

